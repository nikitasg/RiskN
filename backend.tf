terraform {
  backend "s3" {
    bucket         = "nikitasg-terraform-state"
    key            = "RiskN/terraform.tfstate"
    region         = "eu-west-1"
    dynamodb_table = "tfstatelock"
  }
}